cmake_minimum_required(VERSION 3.2 FATAL_ERROR)
project(DemoApp)

set(CMAKE_VERBOSE_MAKEFILE OFF)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON) # force C++14
set(CMAKE_CXX_EXTENSIONS OFF)

# adjust default compiler options
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -Werror")

# 3rd parties
include(${CMAKE_CURRENT_SOURCE_DIR}/third_party/third_parties.cmake)

# source
add_subdirectory(src/DemoApp)
add_subdirectory(src/DemoAppTest)

# unit tests
enable_testing()
add_test(NAME DemoAppTest COMMAND DemoAppTest)
